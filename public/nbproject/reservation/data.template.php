		<div class="container">
			<div class="col-6" id="cliente">
				<input type="text" class="form-control" placeholder="Nombre Completo, Telefono o Correo" name="" />
				<div class="checkbox">
					<input type="checkbox" style="float: right" value="distinguido">
		  			<label style="float: right; margin-right: 25px">
		  				Pasajero distínguido 
		  			</label>
				</div>
				<div class="col-4">
					<span># de servicios</span>
					<select class="form-control col-6">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
					</select>
				</div>
				<div class="col-4">
					<span>Primer Servicio</span>
					<div class="form-control"></div>
				</div>
				<div class="col-4">
					<span>Último Servicio</span>
					<div class="form-control"></div>
				</div>
				<div class="col-6 margen-sup">
					<table class="table table-bordered" style="height: 210px;">
						<tr class="active">
							<td colspan="2">Beneficio Adicional</td>
						</tr>
						<tr>
							<td colspan="2">
								<select class="form-control">
									<option># gratuito</option>
									<option># gratuito</option>
									<option># gratuito</option>
									<option># gratuito</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Acumulados</td>
							<td> </td>
						</tr>
						<tr>
							<td>Usados</td>
							<td> </td>
						</tr>
						<tr>
							<td>Disponibles</td>
							<td> </td>
						</tr>
					</table>
				</div>
				<div class="col-6 margen-sup">
					<table class="table table-bordered" style="height: 210px;">
						<tr class="active">
							<td colspan="2">Convenio</td>
						</tr>
						<tr>
							<td colspan="2">
								<select class="form-control" name="" >
									<option value="Sin Convenio">Sin Convenio</option>
									<option value="------------">------------</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<select class="form-control" name="" >
									<option value="Particular">Particular</option>
									<option value="----------">----------</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>RFC:</td>
							<td>
								<input type="text" class="form-control no-border" name="" />
							</td>
						</tr>
					</table>
				</div>			
				<div class="col-12">
					<textarea rows="4" class="form-control" name="">Notas especiales:</textarea>
				</div>
				
				
			</div>
			<div class="col-6" id="datos">
				<div class="form-control" style="height: 165px;">
					<div class="col-6" style="float: right">
						<div class="col-8">
							<small># de Reservación</small>
						</div>
						<div class="col-4 azul-blanco" id="n_res">
							<p>&nbsp;</p>
						</div>
					</div>
					<form class="form-horizontal" method="post" action="#">
					<div class="form-group col-12 margen-sup">
						<label class="control-label col-2"><small>telefono:</small></label>
						<div class="col-10">
							<input type="text" class="form-control" name="" />
						</div>
					</div>
					<div class="form-group col-12">
						<label class="control-label col-2"><small>email:</small></label>
						<div class="col-10">
							<input type="text" class="form-control" name="" />
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 no-pad cont_direccion">
				<div class="col-12  back_gray">
					<div class="row">
						<div class="col-6">
							Dirección
						</div>
						<div class="col-6" id="icon_direccion" style="float: right">
							(iconos)
						</div>
					</div>
				</div>
		       <div class="tabbable tabs-left">
		        <ul class="nav nav-tabs" id="lat">
		          <li class="active"><a href="#1" data-toggle="tab" class="first">Casa</a></li>
		          <li><a href="#2" data-toggle="tab">Oficina</a></li>
		          <li><a href="#3" data-toggle="tab">Oficina Norte</a></li>
		        </ul>
		        <!--contenidos de las tabs laterales-->
		        <form class="form-horizontal" method="post" action="">
		        	<div class="tab-content content-lateral">
		        	
     					<div class="tab-pane active" id="1"> 
     	<!--contenido de la primera tab-->
					     	<table class="table lat" style="border-top: 1px solid #ddd;">
					     		<tr>
					     			<td colspan="2" class="first">
					     				<input class="back_am form-control" placeholder="Calle y Numero" name="">
					     			</td>
					     		</tr>
					     		<tr>
					     			<td colspan="2">
					     				<input class="back_am form-control" placeholder="Entre que Calles" name="">
					     			</td>
					     		</tr>
					     		<tr>
					     			<td colspan="2">
					     				<input class="back_am form-control" placeholder="Colonia" name="">
					     			</td>
					     		</tr>
					     		<tr>
					     			<td>
					     				<input class="form-control" name="">
					     			</td>
					     			<td>
					     				<input class="form-control" name="">
					     			</td>
					     		</tr>
					     		<tr>
					     			<td colspan="2">
					     				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
					     			</td>
					     		</tr>
					     	</table>
     					</div>
						<div class="tab-pane" id="2">
		         	<!--contenido de la segunda tab-->
				         	<table class="table lat" style="border-top: 1px solid #ddd;">
				         		<tr>
				         			<td colspan="2" class="first">
				         				<input class="form-control back_am" placeholder="Calle y Numero" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<input class="form-control back_am" placeholder="Entre que Calles" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<input class="form-control back_am" placeholder="Colonia" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td>
				         				<input class="form-control" name="">
				         			</td>
				         			<td>
				         				<input class="form-control" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
				         			</td>
				         		</tr>
				         	</table>
		         		</div>
						<div class="tab-pane" id="3">
		         	<!--contenido de la tercera tab-->
		         			<table class="table lat" style="border-top: 1px solid #ddd;">
				         		<tr>
				         			<td colspan="2" class="first">
				         				<input class="form-control back_am" placeholder="Calle y Numero" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<input class="form-control back_am" placeholder="Entre que Calles" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<input class="form-control back_am" placeholder="Colonia" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td>
				         				<input class="form-control" name="">
				         			</td>
				         			<td>
				         				<input class="form-control" name="">
				         			</td>
				         		</tr>
				         		<tr>
				         			<td colspan="2">
				         				<textarea rows="4" class="form-control" name="">Notas Especiales</textarea>
				         			</td>
				         		</tr>
		         			</table>
		         		</div>
					</div>
				</form>
			</div>      
		</div>			
	</div>
</div>
		
		
		
		
		
		
