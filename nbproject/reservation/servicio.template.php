<form class="form-horizontal" method="post" action="#">
	<div class="col-3">
		<div class="col-12 back_gr" style="height: 342px;">
			Tipo de Servicio
			<table class="table table-bordered">
				<tr class="back_white">
					<td colspan="2">
						  <select class="form-control" name="dom">
							<option value="Viaje">Viaje</option>
							<option value="2da opcion">--------</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<select class="form-control" name="dom">
							<option value="Redondo ADA">Redondo ADA</option>
							<option value="Redondo DAD">Redondo DAD</option>
							<option value="Sencillo DA">Sencillo DA</option>
							<option value="Sencillo AD">Sencillo AD</option>
						</select>
					</td>
				</tr>
				<tr class="back_white">
					<td>
						Dom. Adicionales
					</td>
					<td>
						<select class="form-control" name="dom">
							<option value="0">0</option>
							<option value="1">1</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						Pasajeros
					</td>
					<td>
						<select class="form-control" name="dom">
							<option value="0">0&nbsp;&nbsp;</option>
							<option value="1">1&nbsp;&nbsp;</option>
						</select>
					</td>
				</tr>
				<tr class="back_white">
					<td colspan="2">
						<div class="checkbox no-pad">
					    	<label>
					      		<input type="radio" name="reservacion[auto]"> Automovil
					    	</label>
						</div>
						<div class="checkbox no-pad">
					    	<label>
					      		<input type="radio" name="reservacion[auto]"> Camioneta
					    	</label>
					  	</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="col-6">
		<div class="col-12 back_gr">
			Fecha y Hora del Servicio
			<div class="back_white">
				El servicio Apto a Domicilio se requiere el:
				<table class="table table-condensed serv">
					<tbody>
						<tr>
							<td colspan="3" class="col-7">
								<div class="col-12">
									<input type="date" class="form-control" />
								</div>
							</td>
							<td colspan="2" class="col-5">
								<div class="col-12">
									<label class="control-label col-4 no-pad">a las:</label>
									<div class="col-5 no-pad" >
										<input type="time" class="form-control">
									</div>
									<label class="control-label no-pad col-sm-offset-1 col-1">hrs.</label>
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 22%; padding-right: 0px !important;">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">aereolina 1</option>
								  	<option value="2">aereolina 2</option>
								  	<option value="3">aereolina 3</option>
								  	<option value="4">aereolina 4</option>
								  	<option value="5">aereolina 5</option>
								  </select>
								</div>
							</td>
							<td class="no-pad">
								<div class="col-12">
									<input type="text" class="form-control" placeholder="Vuelo" />
								</div>
							</td>
							<td  style="width: 22%" class="no-pad">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">terminal 1</option>
								  	<option value="2">terminal 2</option>
								  	<option value="3">terminal 3</option>
								  	<option value="4">terminal 4</option>
								  	<option value="5">terminal 5</option>
								  </select>
								</div>
							</td>
							<td class="col-3">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">Nacional</option>
								  	<option value="2">Internacional</option>
								  </select>
								</div>
							</td>
							<td class="col-3">
								<div class="col-12">
									<input type="text" class="form-control" placeholder="Hora" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style="margin-top: 66px;">
				El servicio Domicilio a Apto. se requiere el:
				<table class="table table-condensed serv">
					<tbody>
						<tr>
							<td colspan="3" class="col-7">
								<div class="col-12">
									<input type="date" class="form-control" />
								</div>
							</td>
							<td colspan="2" class="col-5">
								<div class="col-12">
									<label class="control-label col-4 no-pad">a las:</label>
									<div class="col-5 no-pad" >
										<input type="time" class="form-control">
									</div>
									<label class="control-label no-pad col-sm-offset-1 col-1">hrs.</label>
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 22%; padding-right: 0px !important;">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">aereolina 1</option>
								  	<option value="2">aereolina 2</option>
								  	<option value="3">aereolina 3</option>
								  	<option value="4">aereolina 4</option>
								  	<option value="5">aereolina 5</option>
								  </select>
								</div>
							</td>
							<td class="no-pad">
								<div class="col-12">
									<input type="text" class="form-control" placeholder="Vuelo" />
								</div>
							</td>
							<td  style="width: 22%" class="no-pad">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">terminal 1</option>
								  	<option value="2">terminal 2</option>
								  	<option value="3">terminal 3</option>
								  	<option value="4">terminal 4</option>
								  	<option value="5">terminal 5</option>
								  </select>
								</div>
							</td>
							<td class="col-3">
								<div class="col-12">
								  <select class="form-control">
								  	<option value="1">Nacional</option>
								  	<option value="2">Internacional</option>
								  </select>
								</div>
							</td>
							<td class="col-3">
								<div class="col-12">
									<input type="text" class="form-control" placeholder="Hora" />
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-3">
		<div class="col-12 back_gr">
			Tarifa
			<table class="table table-bordered table-condensed border0">
				<tr>
					<td>
						<small>Tarifa aplicada</small>
					</td>
					<td>
						<p class="text-right"> <small>$ 456.00</small></p>
					</td>
				</tr>
				<tr class="back_white">
					<td>
						<small>Dom. adic.</small>
					</td>
					<td>
						<p class="text-right"><small>$ 0.00</small></p>
					</td>
				</tr>
				<tr>
					<td>
						<div class="checkbox">
						  <label>
						    <input type="checkbox" value="">
						    <small>Beneficio</small>
						  </label>
						</div> 
					</td>
					<td>
						<p class="text-right" style="color:red;"><small>$ -123.00</small></p>
					</td>
				</tr>
				<tr class="back_white">
					<td>
						<div class="checkbox">
						  <label>
						    <input type="checkbox" value="">
						    <small>Ajuste Manual</small>
						  </label>
						</div>  
					</td>
					<td>
						<p class="text-right"><small>$ 0.00</small></p>
					</td>
				</tr>
				<tr>
					<td class="border0">
						<p class="text-right"><small>TOTAL:</small></p>
					</td>
					<td>
						<p class="text-right"><small> $ 333.00</small></p>
					</td>
				</tr>
			</table>
			<select class="form-control" name="">
				<option>Forma de pago</option>
				<option>efectivo</option>
				<option>tarjeta</option>
			</select>
			<div class="checkbox col-8" style="float: right">
				<label>
		          <input name="" type="checkbox"> Imprimir leyenda: Bajo Riesgo del usuario.
		        </label>
			</div>
			<div class="form-group">
			    <div class="col-12">
			      <button type="submit" class="btn buttonres btn-success">Generar Reservación</button>
			    </div>
			</div>
		</div>
	</div>
	<div class="col-9" style="margin-top: -50px;">
		<div class="col-12  back_gr">
			<textarea class="form-control" rows="3">Notas Especiales:</textarea>
		</div>
	</div>
	<div class="col-12 back_gr margen-sup">
		Estado de la reservación					
	</div>
	<div class="back_gr col-12" style="margin-bottom:10px" >
		<div class="col-10" style="margin-left:5%">
			<div class="col-2">
				Status:
			</div>
			<div class="col-2">
				Elaboró:
			</div>
			<div class="col-2">
				Creada:
			</div>
			<div class="col-2">
				Vía:
			</div>
			<div class="col-2">
				Editó:
			</div>
			<div class="col-2">
				Editada:
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
			<div class="col-2">
				<select class="form-control estado">
					<option>En elaboración</option>
				</select>
			</div>
		</div>
	</div>
</form>