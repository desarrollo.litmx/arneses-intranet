<?php

class sessionDriver extends driverBase {        
    
    private static $instance;
    
    private function __construct() {
        if(!self::sessionStarted()) {
           // $handler = new sesHandler();
            //session_set_save_handler($handler, true); //almacena o recupera información en una base de datos relacionada con la sesión de un usuario
            session_start(); //inicia una nueva sesión o reanuda la existente
        }
            
    }
    
    public static function sessionInstance() {
        if( !self::$instance instanceof self ) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    //inicio de sesión
    public static function sessionStarted() {
        if(session_id() == '') {
            return false;
        } else {
            return true;
        }
    }
    //si la sesión se ha iniciado 
    public static function sessionExists($session) {
    	
        if(self::sessionStarted() == false) {
            $s = self::sessionInstance();
        }
        if(isset($_SESSION[$session])) {
            return true;
        } else {
            return false;
        }
    }
    //marca que la sesión no esta activa
    public static function setSession($session, $value) {
        if(self::sessionStarted() != true) {
            $s = self::sessionInstance();
        }
        $_SESSION[$session] = $value;
        if(self::sessionExists($session) == false) {
            throw new Exception('Unable to Create Session');
        }
    }
    
    public static function getSession($session) {
        if(self::sessionStarted() != true) {
            $s = self::sessionInstance();
        }
        if(isset($_SESSION[$session])) {
            return $_SESSION[$session];
        } 
        
        throw new Exception("Variable de session {$session} no existe.");
    }
//cerrar sesión
    public static  function close() {
        $s = self::sessionInstance();
        session_destroy();
    }
        
}

