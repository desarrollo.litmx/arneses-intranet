<header class='row'>
    <a href="/" class="alogo"><img class="logo" src="/static/images/INTRA/KSW2_LOGOARNESES.png" /></a>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logob" src="/static/images/INTRA/KSW2_LOGOARNESES.png" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href='/'>INICIO</a></li>
                    <li><a href='/menu'>MENÚ</a></li>
                    <li class='active'><a href='/sews'>SEWS MÉXICO</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="container">
    <section class='row'>
        <section class='col-xs-12 col-sm-4 col-md-4'>
            <ul class="lmenu">
                <br />
                <br />
                <li><a href='/'>INICIO</a></li>
                <br />
                <li><a href='/menu'>MENÚ</a></li>
                <br />
                <li class='active'><a href='/sews'>SEWS MÉXICO</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-minus" aria-hidden="true"></span></li>
                <br />
            </ul>
        </section>
        <section class='col-xs-12 col-sm-8 col-md-8'>
            <ul>
                <li>SEWS MÉXICO</li>
            </ul>
            <br />
            <br />
            <section class="col-xs-12 firstdiv">
                <ul class="lsubmenu">
                    <li><a href='https://www.ksmex.com.mx/kys_compras_sews/'>COMPRAS SEWS</a></li>
                    <br />
                    <li><a href='https://www.ksmex.com.mx/kys_cr_sews/'>CR_SEWS</a></li>
                </ul>
            </section>
        </section>
    </section>
</section>
<footer style='position: fixed; bottom: 0;'>
    <section class='row'>
        <section class="col-xs-12 col-sm-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12 col-sm-12"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a>
            </section>
        </section>
        <section class="col-xs-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12">
                ⓒ Copyright 2019 Todos Los Derechos Reservados.
            </section>
        </section>
   </section>
</footer>