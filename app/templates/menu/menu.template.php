<header class='row'>
    <a href="/" class="alogo"><img class="logo" src="/static/images/INTRA/KSW2_LOGOARNESES.png" /></a>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logob" src="/static/images/INTRA/KSW2_LOGOARNESES.png" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href='/'>INICIO</a></li>
                    <li class='active'><a href='/menu'>MENÚ</a></li>
                    <!--<li><a href='/sews'>SEWS MÉXICO</a></li>-->
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="container">
    <section class='row'>
        <section class='col-xs-12 col-sm-4 col-md-4'>
            <ul class="lmenu">
                <br />
                <br />
                <li><a href='/'>INICIO</a></li>
                <br />
                <li class='active'><a href='/menu'>MENÚ</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-minus" aria-hidden="true"></span></li>
                <br />
                <!--<li><a href='/sews'>SEWS MÉXICO</a></li>
                <br />-->
            </ul>
        </section>

        <section class='col-xs-12 col-sm-8 col-md-8' style='    margin-top: -50px;'>
        <section class='col-xs-12'>
                <ul>
                    <li>MENÚ K&S MEXICANA</li>
                </ul>
                <br />
                <section class="col-xs-12 col-md-6 firstdiv">
                    <ul class="lsubmenu">
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_ccd_2/' target="_blank" >CCD SYSTEM V2.0</a></li>
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_ccd_3/' target="_blank" >CCD SYSTEM V3.0</a></li>
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_ccd_4/' target="_blank" >CCD SYSTEM V4.0</a></li>
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_ts/' target="_blank" >CTRL DOCUMENTOS</a></li>
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_pots/' target="_blank" >POTS SYSTEM</a></li>
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_cex/' target="_blank" >CONTROL ADUANAL</a></li> 
                       <!--<li><a href='https://srvagslnx06.ksmex.com.mx/kys_compras/' target="_blank" >COMPRAS</a></li>-->
                    </ul>
                </section>
                <section class="col-xs-12 col-md-6 tdiv">
                        <ul class="lsubmenu">
                            <li><a href='https://srvagslnx06.ksmex.com.mx/kys_mps/' target="_blank" >MPS SYSTEMS</a></li>
                            <li><a href='https://srvagslnx06.ksmex.com.mx/kys_docs/' target="_blank" >K&S DOCS</a></li>
                            <li><a href='https://srvagslnx06.ksmex.com.mx/kys_c_tpat/' target="_blank" >C-TPAT</a></li>
                           <!-- <li><a href='https://srvagslnx06.ksmex.com.mx/kys_agenda/' target="_blank" >AGENDA</a></li>
                            --><li><a href='https://srvagslnx06.ksmex.com.mx/kys_cs/' target="_blank" >CS SYSTEMS</a></li>
                            <li><a href="https://srvagslnx06.ksmex.com.mx/kys_compras/" target="_blank">COMPRAS</a></li>
                            <li><a href="https://srvagslnx06.ksmex.com.mx/kys_fac/" target="_blank">CONTROL INVENTARIOS</a></li>
                      </ul>
                </section>
            </section>
            <section class='col-xs-12'>
                <ul>
                    <li>MENÚ SEWS MÉXICO</li>
                </ul>
                <br />
                <section class="col-xs-12 col-md-6 firstdiv">
                    <ul class="lsubmenu">
                        <li><a href='https://srvagslnx06.ksmex.com.mx/kys_compras_sews/' target="_blank" >COMPRAS SEWS</a></li>
                    </ul>
                </section>
                <section class="col-xs-12 col-md-6 tdiv">
                        <ul class="lsubmenu">
                            <li><a href='https://srvagslnx06.ksmex.com.mx/kys_docs_sews/' target="_blank" >SEWS DOCS</a></li>
                        </ul>
                </section>
            </section>
        </section>
        
    </section>
</section>
<!--<footer style='bottom: 0;position:'>
    <section class='row'>
        <section class="col-xs-12 col-sm-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12 col-sm-12"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a>
            </section>
        </section>
        <section class="col-xs-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12">
                ⓒ Copyright 2019 Todos Los Derechos Reservados.
            </section>
        </section>
   </section>
</footer>-->
