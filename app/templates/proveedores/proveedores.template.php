<header class='row'>
    <a href="/"><img class="logo" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
</header>
<section class="container">
<section class='row'>
    <section class='col-xs-5'>
    <ul class="lmenu">
    <br />
        <br />
        <li><a href='/'>INICIO</a></li>
        <br />
        <li><a href='/mision'>MISIÓN</a></li>
        <br />
        <li class="active"><a href='/vision'>VISIÓN</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-minus" aria-hidden="true"></span></li>
        <br />
        <li><a href='/politica'>POLITICA DE CALIDAD</a></li>
        <br />
        <li><a href='/vacantes'>VACANTES</a></li>
        <br />
        <li><a href='/seguridad'><button class="btn btn-proveedores">SEGURIDAD-CONTRATISTAS</button></a></li>
        <br />
    </ul>
    </section>
    <section class='col-xs-7'>
        <ul class="smenu">
            <br />
            <li><a id="tit" href='#'>PROVEEDORES</a></li>
            <br /><br /><br />
            <li><a href='#'>REGISTRO DE EMPRESA</a></li>
            <br /><br />
            <li><a href='#'>REGISTRO DE TRABAJADORES</a></li>
            <br /><br />
            <li><a href='#'>LOGIN Y CAPACITACIÓN DE TRABAJADOR</a></li>
            <br /><br />
            <li><a href='#'>MÓDULO DE MONITOREO</a></li>
            <br />
            <br />
        </ul>
    </section>
</section>
</section>
<footer style='position: fixed; bottom: 0;'>
    <section class='row'>
        <section class="col-xs-12 col-sm-12" style='text-align:center; font-size:18px;'>
            <section class="col-xs-12 col-sm-12"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a></section>
        </section>
        <section class="col-xs-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12">ⓒ Copyright 2019 Todos Los Derechos Reservados.</section>
        </section>
    </section>
</footer>


<!--public





-->